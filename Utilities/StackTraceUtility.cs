﻿using System.Diagnostics;
using System.Text;

namespace UnityLogging.Utilities
{
    internal static class StackTraceUtility  
    {
        internal static string GetFormattedStackTrace(StackTrace stackTrace)
        {
            var stringBuilder = new StringBuilder((int) byte.MaxValue);
            for (var index1 = 0; index1 < stackTrace.FrameCount; ++index1)
            {
                var frame = stackTrace.GetFrame(index1);
                var method = frame.GetMethod();
                if (method == null) 
                    continue;
                
                var declaringType = method.DeclaringType;
                if (declaringType == null) 
                    continue;
                
                var declaringTypeNamespace = declaringType.Namespace;
                if (!string.IsNullOrEmpty(declaringTypeNamespace))
                {
                    if (declaringType.Name != typeof(Examples.LogExample).Name &&
                        (declaringTypeNamespace.StartsWith("UnityLogging") ||
                         declaringTypeNamespace.StartsWith("UnityEngine")))
                    {
                        continue;
                    }
                    
                    //Namespace
                    stringBuilder.Append(declaringTypeNamespace);
                    stringBuilder.Append(".");
                }

                //Type
                stringBuilder.Append(declaringType.Name);
                stringBuilder.Append(":");
                
                //Method
                stringBuilder.Append(method.Name);
                
                //Parameters
                stringBuilder.Append("(");
                var index2 = 0;
                var parameters = method.GetParameters();
                var flag = true;
                for (; index2 < parameters.Length; ++index2)
                {
                    if (!flag)
                        stringBuilder.Append(", ");
                    else
                        flag = false;
                    stringBuilder.Append(parameters[index2].ParameterType.Name);
                }
                stringBuilder.Append(")");
                
                //File
                var fileName = frame.GetFileName();
                if(fileName == null)
                    break;
                
                stringBuilder.Append(" (at ");
                
                //Shorten from absolute to relative path
                var dataPath = UnityEngine.Application.dataPath.Replace("\\", "/").Replace("Assets", "");
                if (!string.IsNullOrEmpty(dataPath) && fileName.Replace("\\", "/").StartsWith(dataPath))
                    fileName = fileName.Substring(dataPath.Length, fileName.Length - dataPath.Length).Replace("\\", "/");
                    
                stringBuilder.Append(fileName);
                stringBuilder.Append(":");
                
                //Line number
                stringBuilder.Append(frame.GetFileLineNumber().ToString());
                stringBuilder.Append(")");
                
                //End
                stringBuilder.Append("\n");
                
                break;
            }

            return stringBuilder.ToString();
        }
    }
}
