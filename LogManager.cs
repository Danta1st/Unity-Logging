﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using UnityLogging.Loggers;
using UnityLogging.Writers;
using ILogger = UnityLogging.Loggers.ILogger;
using Logger = UnityLogging.Loggers.Logger;

namespace UnityLogging
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public static class LogManager
    {
        private static LogLevel logLevel = LogLevel.Fatal;
        private static readonly Dictionary<string, ILogger> Loggers = new Dictionary<string, ILogger>();
        private static readonly HashSet<ILogWriter> Writers = new HashSet<ILogWriter>();

        // ReSharper disable once ParameterHidesMember
        public static void Initialize(LogLevel logLevel)
        {
            SetLogLevel(logLevel);

            var fileLogWriter = new FileLogWriter("Player");
            AddWriter(fileLogWriter);

            //TODO: Add conditional for when this is included in builds
            var runtimeLogViewWriter = RuntimeLogViewWriter.Factory.Create();
            AddWriter(runtimeLogViewWriter);

            AddUnityAdapterLogger();
        }

        private static void AddUnityAdapterLogger()
        {
            var unityAdapterLogger = new UnityAdapterLogger {LogLevel = logLevel};
            RegisterLogger<UnityAdapterLogger>(unityAdapterLogger);
        }

        /// <summary>
        /// Utility method to get a default logger with context tag of the provided type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static ILogger GetLogger<T>()
        {
            var context = typeof(T).Name;
            
            return GetLogger(context);
        }

        /// <summary>
        /// Utility method to get a default logger with the provided context tag.
        /// </summary>
        /// <param name="context"></param>
        /// <returns>An <see cref="ILogger"/> object complying with the global loglevel</returns>
        public static ILogger GetLogger(string context)
        {
            ILogger logger;
            if (Loggers.TryGetValue(context, out logger)) 
                return logger;

            logger = new Logger(context) {LogLevel = logLevel};
            RegisterLogger<Logger>(logger);

            return logger;
        }

        /// <summary>
        /// Utility method to register a custom logger with the system.
        /// The <see cref="LogManager"/> uses this method internally.
        /// This will probably never be publicly necessary unless you are listening to events from an external source,
        /// and want to pipe these into the system.
        /// </summary>
        /// <param name="logger"></param>
        /// <typeparam name="T"></typeparam>
        public static void RegisterLogger<T>(ILogger logger)
        {
            var context = typeof(T).Name;
            logger.OnLogMessage += OnLogMessage;
            Loggers.Add(context, logger);
        }

        private static void OnLogMessage(IMessage message)
        {
            foreach (var writer in Writers)
            {
                writer.Write(message);
            }
        }

        /// <summary>
        /// Utility method to set the global loglevel.
        /// Will overwrite all individual log levels.
        /// </summary>
        /// <param name="logLevel"></param>
        public static void SetLogLevel(LogLevel logLevel)
        {
            LogManager.logLevel = logLevel;
            
            foreach (var kvp in Loggers)
            {
                kvp.Value.LogLevel = logLevel;
            }
        }

        /// <summary>
        /// Utility method to register a custom writer with the system.
        /// The <see cref="LogManager"/> uses this method internally.
        /// Create and register your own <see cref="ILogWriter"/> in order to pipe log events to
        /// a custom location such as Monotoring-, Analysis-, or other service.
        /// </summary>
        /// <param name="logWriter">The concrete writer to add</param>
        public static void AddWriter(ILogWriter logWriter)
        {
            Writers.Add(logWriter);
        }
    }
}