﻿using System;
using System.Diagnostics;

namespace UnityLogging
{
    public interface IMessage
    {
        LogLevel LogLevel { get; }
        DateTime DateTime { get; }
        string Context { get; }
        string Content { get; }
        string StackTrace { get; }
    }
    
    public class Message : IMessage
    {
        public LogLevel LogLevel { get; private set; }
        public string Context { get; private set; }
        public string Content { get; private set; }
        public DateTime DateTime { get; private set; }
        public string StackTrace { get; private set; }

        public Message(LogLevel logLevel, string context, object content)
        {
            LogLevel = logLevel;
            Context = context;
            Content = content == null ? "null" : content.ToString();
            
            DateTime = DateTime.Now;
            
            var stackTrace = new StackTrace(3, true);
            StackTrace = Utilities.StackTraceUtility.GetFormattedStackTrace(stackTrace);
        }

        public override string ToString()
        {
            var content = LogLevel == LogLevel.Fatal
                ? Content + Environment.NewLine + StackTrace
                : Content;  
            
            var formattedMessage = string.Format("{0:HH:mm:ss.ffff} [{1}] {2}", 
                DateTime, 
                Context, 
                content);

            return formattedMessage;
        }
    }

    public static class MessageExtensions
    {
    }
}