﻿using System;
using UnityEngine;

namespace UnityLogging.Loggers
{
    /// <summary>
    /// Listens on the <see cref="Application.logMessageReceived"/> event.
    /// When the event is fired this logger will translate and forward
    /// traditional unity Debug.Log and its variants to the attached writers. 
    /// </summary>
    internal sealed class UnityAdapterLogger : Logger
    {
        internal UnityAdapterLogger() : base(typeof(UnityAdapterLogger).Name)
        {
            Application.logMessageReceived += ApplicationOnLogMessageReceived;
        }

        private void ApplicationOnLogMessageReceived(string condition, string stacktrace, LogType type)
        {
            switch (type)
            {
                case LogType.Error:
                    Error(condition);
                    break;
                
                case LogType.Assert:
                    Debug(condition);
                    break;
                
                case LogType.Warning:
                    Warning(condition);
                    break;
                
                case LogType.Log:
                    Verbose(condition);
                    break;
                
                case LogType.Exception:
                    Fatal(condition + Environment.NewLine + stacktrace);
                    break;
                
                default:
                    throw new ArgumentOutOfRangeException("type", type, null);
            }
        }

        ~UnityAdapterLogger()
        {
            Application.logMessageReceived -= ApplicationOnLogMessageReceived;
        }
    }
}