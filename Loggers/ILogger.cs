﻿using System;

namespace UnityLogging.Loggers
{
    public interface ILogger
    {
        LogLevel LogLevel { set; }

        event Action<IMessage> OnLogMessage; 
        
        //TODO: Document log levels based on the documentation from the enum.
        void Fatal(object content);
        void Error(object content);
        void Warning(object content);
        void Information(object content);
        void Debug(object content);
        void Verbose(object content);
    }
}