﻿using System;

namespace UnityLogging.Loggers
{
    internal class Logger : ILogger
    {
        public LogLevel LogLevel { private get; set; }
        public event Action<IMessage> OnLogMessage = delegate {};
        private readonly string context;

        public Logger(string context)
        {
            this.context = context;
        }

        public void Fatal(object content)
        {
            var message = GetMessage(content, LogLevel.Fatal);
            Log(message);
        }

        public void Error(object content)
        {
            var message = GetMessage(content, LogLevel.Error);
            Log(message);
        }

        public void Warning(object content)
        {
            var message = GetMessage(content, LogLevel.Warning);
            Log(message);
        }

        public void Information(object content)
        {
            var message = GetMessage(content, LogLevel.Information);
            Log(message);
        }

        public void Debug(object content)
        {
            var message = GetMessage(content, LogLevel.Debug);
            Log(message);
        }

        public void Verbose(object content)
        {
            var message = GetMessage(content, LogLevel.Verbose);
            Log(message);
        }

        private IMessage GetMessage(object content, LogLevel logLevel)
        {
            return new Message(logLevel, context, content);
        }

        private void Log(IMessage message)
        {
            if(message.LogLevel > LogLevel)
                return;

            OnLogMessage(message);
        }
    }
}