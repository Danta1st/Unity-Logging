﻿using System;
using UnityEngine;
using ILogger = UnityLogging.Loggers.ILogger;

namespace UnityLogging.Examples
{
    public class LogExample : MonoBehaviour
    {
        private ILogger logger;
        private void Awake()
        {
            LogManager.Initialize(LogLevel.Verbose);
            logger = LogManager.GetLogger<LogExample>();
        
            SpawnLogOutput();
        }

        private void Update()
        {
            if (Input.GetKey(KeyCode.T))
            {
                SpawnLogOutput();
            }
            
            if(Input.GetKeyDown(KeyCode.E))
                throw new Exception("MyExceptionMessage");
        }

        private void SpawnLogOutput()
        {
            logger.Verbose("Verbose");
            logger.Debug("Debug - plain text");
            logger.Information("Information");
            logger.Warning("Warning");
            logger.Error("Error");
            logger.Fatal("Fatal");

            logger.Debug(new {Message = "Debug - Custom object"});

            Debug.Log("Unitylog");
            Debug.LogWarning("UnityWarning");
            Debug.LogError("UnityError");
        }
    }
}
