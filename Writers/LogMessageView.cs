﻿using UnityEngine;
using UnityEngine.UI;
using ColorUtility = UnityLogging.Utilities.ColorUtility;

namespace UnityLogging.Writers
{
    internal class LogMessageView : MonoBehaviour
    {
        [SerializeField] private Text text;
        
        public void SetContent(IMessage message)
        {
            text.text = message.ToString();
            var color = ColorUtility.GetLogLevelColor(message.LogLevel, Color.white);
            text.color = color;

            transform.SetAsLastSibling();
        }
    }
}