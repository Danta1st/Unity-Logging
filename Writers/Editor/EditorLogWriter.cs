﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEditor;
using UnityEngine;

namespace UnityLogging.Writers.Editor
{
    internal sealed class EditorLogWriter : EditorWindow, ILogWriter
    {
        [MenuItem("Window/uConsole")]
        private static void Initialize()
        {
            var window = GetWindow<EditorLogWriter>();
            window.Show();
        }

        private void OnEnable()
        {
            titleContent = EditorGUIUtility.IconContent("UnityEditor.ConsoleWindow");
            titleContent.text = "uConsole";
            
            defaultBackgroundColor = GUI.backgroundColor;
            
            LogManager.AddWriter(this);
        }

        private void OnInspectorUpdate()
        {
            Repaint();
        }

        private readonly Queue<IMessage> messages = new Queue<IMessage>();
        private readonly Filter filter = new Filter();
        private Color defaultBackgroundColor = Color.clear;
        private Vector2 scrollPosition = Vector2.zero;
        
        private void OnGUI()
        {
            GUILayout.Space(5);
            
            DrawHeaderToolbar();

            GUILayout.Space(5);
            
            DrawLogsView();
        }

        private void DrawHeaderToolbar()
        {
            EditorGUILayout.BeginHorizontal(GUILayout.Height(16), GUILayout.ExpandWidth(true));
            {
                GUILayout.Space(5);
                
                DrawLogLevelFilter();

                DrawContextFilterField();

                DrawMessageFilterField();

                DrawStackTraceToggle();

                DrawClearLogButton();

                DrawOpenFileButton();
                
                GUILayout.Space(5);
            }
            EditorGUILayout.EndHorizontal();
        }

        private void DrawLogLevelFilter()
        {
            var newLogLevelFilter = (LogLevel) EditorGUILayout.EnumPopup(filter.LogLevel, GUILayout.Width(128), GUILayout.ExpandHeight(true));
            if (newLogLevelFilter == filter.LogLevel) 
                return;
            
            filter.LogLevel = newLogLevelFilter;
            LogManager.SetLogLevel(filter.LogLevel);
        }

        private void DrawContextFilterField()
        {
            filter.Context = EditorGUILayout.TextField(filter.Context, GUILayout.Width(160), GUILayout.ExpandHeight(true));
        }

        private void DrawMessageFilterField()
        {                
            filter.Content = EditorGUILayout.TextField(filter.Content, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
        }

        private void DrawStackTraceToggle()
        {
            filter.StackTracke = EditorGUILayout.ToggleLeft("StackTrace", filter.StackTracke, GUILayout.Width(98));
        }

        private void DrawClearLogButton()
        {
            if (GUILayout.Button("Clear", GUILayout.Width(64), GUILayout.ExpandHeight(true)))
            {
                ClearLog();
            }
        }

        private void ClearLog()
        {
            messages.Clear();
        }

        private static void DrawOpenFileButton()
        {
            //TODO: Verify open button functionality on osx, and linux environments.
            if (GUILayout.Button("Open File", GUILayout.Width(64), GUILayout.ExpandHeight(true)))
            {
                Process.Start(@Application.persistentDataPath);
            }
        }

        
        private void DrawLogsView()
        {
            scrollPosition = GUILayout.BeginScrollView(scrollPosition);
            {
                EditorGUILayout.BeginVertical();
                {
                    foreach (var message in messages)
                    {
                        DrawMessageLog(message);
                    }
                }
                EditorGUILayout.EndVertical();
            }
            GUILayout.EndScrollView();
        }

        private void DrawMessageLog(IMessage message)
        {
            if(message.LogLevel > filter.LogLevel)
                return;
            
            if(!string.IsNullOrEmpty(filter.Context) && 
               !message.Context.ToLower().Contains(filter.Context.ToLower()))
                return;
            
            if(!string.IsNullOrEmpty(filter.Content) && 
               !message.Content.ToLower().Contains(filter.Content.ToLower())) 
                return;
            
            var wordWrappedStyle = GUI.skin.label;
            wordWrappedStyle.wordWrap = true;

            var color = Utilities.ColorUtility.GetLogLevelColor(message.LogLevel, defaultBackgroundColor);
            GUI.backgroundColor = color;

            EditorGUILayout.BeginHorizontal("Box");
            {
                EditorGUILayout.LabelField(message.DateTime.ToString("HH:mm:ss.ffff"), GUILayout.Width(128));
                EditorGUILayout.LabelField(message.Context, GUILayout.Width(160));
                EditorGUILayout.LabelField(GetFormattedContent(message), wordWrappedStyle, GUILayout.ExpandWidth(true));
            }
            EditorGUILayout.EndHorizontal();

            GUI.backgroundColor = defaultBackgroundColor;
        }

        private string GetFormattedContent(IMessage message)
        {
            var content = filter.StackTracke || message.LogLevel == LogLevel.Fatal 
                ? message.Content + Environment.NewLine + message.StackTrace 
                : message.Content;
            
            return content;
        }

        void ILogWriter.Write(IMessage message)
        {
            if (messages.Count > 500)
            {
                RemoveNotification();
                ShowNotification(new GUIContent("Maximum simultaneous logs reached. \n\nRemoving by rule: first in, first out."));
                messages.Dequeue();
            }

            messages.Enqueue(message);
            scrollPosition = new Vector2(scrollPosition.x, Mathf.Infinity);
            Repaint();
        }

        private sealed class Filter
        {
            public LogLevel LogLevel = LogLevel.Verbose;
            public string Context;
            public string Content;
            public bool StackTracke;
        }
    }
}