﻿using System.IO;
using System.Text;
using UnityEngine;

namespace UnityLogging.Writers
{
    /// <summary>
    /// This writer writes to a custom log file within <see cref="Application.persistentDataPath"/>.
    /// A total of two consecutive logs are kepth. The current and the previous.
    /// </summary>
    internal sealed class FileLogWriter : ILogWriter
    {
        private readonly StreamWriter streamWriter;

        public FileLogWriter(string fileName)
        {
            var logFilePath = Path.Combine(Application.persistentDataPath, fileName + ".log");
            var prevLogFilePath = Path.Combine(Application.persistentDataPath, fileName + "-prev.log");
            
            if(File.Exists(logFilePath))
                File.Copy(logFilePath, prevLogFilePath, true);
            
            streamWriter = new StreamWriter(logFilePath, false, Encoding.UTF8);
        }

        public void Write(IMessage message)
        {
            var msg = message.ToString();
            
            streamWriter.WriteLine(msg);
            streamWriter.Flush();
        }
    }
}