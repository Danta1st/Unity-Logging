﻿using System;
using UnityLogging.Loggers;

namespace UnityLogging.Writers
{
    /// <summary>
    /// Writes to the default <see cref="Console.Out"/>.
    /// When within the Unity Editor, this will be the default editor log file.
    /// When within a unity player, this will be the default player log file.
    /// These logs will not be visible within the unity default console window.
    /// </summary>
    internal sealed class UnityLogWriter : ILogWriter
    {
        private static readonly object SyncObject = new object();
        
        public void Write(IMessage message)
        {
            //Unity internally writes to the editor log file. We don't want duplicates.
            if(message.Context == typeof(UnityAdapterLogger).Name)
                return;
            
            lock (SyncObject)
            {
                var msg = message.ToString();
                Console.WriteLine(msg);
            }
        }
    }
}