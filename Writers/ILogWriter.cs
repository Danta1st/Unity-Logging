﻿namespace UnityLogging.Writers
{
    public interface ILogWriter
    {
        void Write(IMessage message);
    }
}