﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UnityLogging.Writers
{
    internal class RuntimeLogViewWriter : MonoBehaviour, ILogWriter
    {
        [Header("Dependencies - Prefabs")]
        [SerializeField] private LogMessageView logMessageViewPrefab;

        [Header("Dependencies - local")] 
        [SerializeField] private Toggle logViewToggle;
        [SerializeField] private ScrollRect scrollRect;
        
        private readonly Queue<LogMessageView> messageViews = new Queue<LogMessageView>(500);

        private void Awake()
        {
            logViewToggle.onValueChanged.AddListener(arg0 =>
            {
                scrollRect.gameObject.SetActive(arg0);
            });
            
            logViewToggle.isOn = false;
        }

        public void Write(IMessage message)
        {
            var messageView = messageViews.Count >= 500 
                ? messageViews.Dequeue() 
                : Instantiate(logMessageViewPrefab, scrollRect.content);
            
            messageView.SetContent(message);


            if (message.LogLevel == LogLevel.Fatal)
            {
                SnapTo(messageView.transform as RectTransform);
                logViewToggle.isOn = true;
            }
            else
            {
                scrollRect.verticalNormalizedPosition = 0;
            }
        }

        private void SnapTo(RectTransform rectTransform)
        {
            Canvas.ForceUpdateCanvases();

            var yPos = ((Vector2)scrollRect.transform.InverseTransformPoint(scrollRect.content.position)
                                       - (Vector2)scrollRect.transform.InverseTransformPoint(rectTransform.position)).y;
            
            scrollRect.content.anchoredPosition = new Vector2(scrollRect.content.position.x, yPos);
        }
        
        public static class Factory
        {
            public static ILogWriter Create()
            {
                var path = "Logging/" + typeof(RuntimeLogViewWriter).Name;
                var prefab = Resources.Load<RuntimeLogViewWriter>(path);
                var instance = Instantiate(prefab);
                
                DontDestroyOnLoad(instance.gameObject);
                
                return instance;
            }
        }
    }
}
